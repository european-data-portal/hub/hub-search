package io.piveau.hub.search.util;

import io.piveau.hub.search.util.utils.Field;
import io.piveau.hub.search.util.utils.geo.BoundingBox;
import io.piveau.hub.search.util.utils.geo.Spatial;
import io.piveau.hub.search.models.Constants;
import io.vertx.core.*;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.http.HttpHost;
import org.elasticsearch.ElasticsearchStatusException;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.mapping.put.PutMappingRequest;
import org.elasticsearch.action.admin.indices.settings.put.UpdateSettingsRequest;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.*;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.document.DocumentField;
import org.elasticsearch.common.geo.builders.EnvelopeBuilder;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.*;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.index.reindex.UpdateByQueryRequest;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.global.Global;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedStringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.tophits.ParsedTopHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.ScoreSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.locationtech.jts.geom.Coordinate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.elasticsearch.index.query.QueryBuilders.geoShapeQuery;

public class ElasticsearchConnector {

    private static final Logger LOG = LoggerFactory.getLogger(ElasticsearchConnector.class);

    // type -> fields
    private static final Map<String, Map<String, Field>> fields = new HashMap<>();

    // type -> facets
    private static final Map<String, Map<String, ImmutablePair<String, String>>> facets = new HashMap<>();

    // type -> facetOrder
    private static final Map<String, List<String>> facetOrder = new HashMap<>();

    // type -> searchParams
    private static final Map<String, Map<String, String>> searchParams = new HashMap<>();

    private RestHighLevelClient client;

    private JsonObject config;

    // defines the maximum size of an aggregation
    private int max_agg_size;

    // defines the maximum result of from + size
    private int max_result_window;

    // vertx context
    private Vertx vertx;

    public static ElasticsearchConnector create(Vertx vertx, JsonObject config,
                                                Handler<AsyncResult<ElasticsearchConnector>> handler) {
        return new ElasticsearchConnector(vertx, config, handler);
    }

    private ElasticsearchConnector(Vertx vertx, JsonObject config, Handler<AsyncResult<ElasticsearchConnector>> handler) {
        this.vertx = vertx;

        this.config = config;

        String host = config.getString("host", "localhost");
        Integer port = config.getInteger("port", 9200);

        this.max_agg_size = config.getInteger("max_agg_size", 50);

        this.max_result_window = config.getInteger("max_result_window", 10000);

        this.client = new RestHighLevelClient(
                RestClient.builder(new HttpHost(host, port, "http"))
        );

        // Sniffer sniffer = Sniffer.builder(client.getLowLevelClient()).setSniffIntervalMillis(60000).build();
        // sniffer.sniffOnFailure();

        /*for (Node n : client.getLowLevelClient().getNodes()) {
            LOG.debug(n.toString());
        }*/

        JsonArray indexes = config.getJsonArray("index");
        if (indexes == null || indexes.isEmpty()) {
            handler.handle(Future.failedFuture("Index config is missing!"));
        } else {
            List<Future> indexFutureList = initIndexes(indexes);
            CompositeFuture.join(indexFutureList).setHandler(indexFutureHandler -> {
                if (indexFutureHandler.succeeded()) {
                    setMaxResultWindow
                            (max_result_window, ar -> {
                                if (ar.succeeded()) {
                                    handler.handle(Future.succeededFuture(this));
                                } else {
                                    LOG.error("Init indexes: " + indexFutureHandler.cause());
                                    handler.handle(Future.succeededFuture(this));
                                    // handler.handle(Future.failedFuture(ar.cause()));
                                }
                            });
                } else {
                    LOG.error("Init indexes: " + indexFutureHandler.cause());
                    handler.handle(Future.succeededFuture(this));
                    // handler.handle(Future.failedFuture(indexFutureHandler.cause()));
                }
            });
        }
    }

    private List<Future> initIndexes(JsonArray indexes) {
        List<Future> indexFutureList = new ArrayList<>();
        for (Object index : indexes) {
            Future<Void> indexFuture = Future.future();
            indexFutureList.add(indexFuture);

            JsonObject indexJson = (JsonObject) index;
            String type = indexJson.getString("type");
            String indexSettingsFilepath = indexJson.getString("settings");
            String typeMappingFilepath = indexJson.getString("mapping");
            JsonArray typeFacets = indexJson.getJsonArray("facets");
            JsonArray typeSearchParams = indexJson.getJsonArray("searchParams");

            if (type == null || indexSettingsFilepath == null || typeMappingFilepath == null) {
                indexFuture.fail("Index config incorrect!");
            } else {
                facets.putIfAbsent(type, new HashMap<>());
                List<String> facetOrderList = new ArrayList<>();
                if (typeFacets != null) {
                    for (Object facet : typeFacets) {
                        JsonObject facetJson = (JsonObject) facet;
                        String facetName = facetJson.getString("name");
                        String facetTitle = facetJson.getString("title");
                        String facetPath = facetJson.getString("path");
                        if (facetName == null || facetPath == null) {
                            indexFuture.fail("Index config incorrect!");
                            break;
                        }
                        facets.get(type).putIfAbsent(facetName, new ImmutablePair<>(facetTitle, facetPath));
                        facetOrderList.add(facetName);
                    }
                }
                facetOrder.putIfAbsent(type, facetOrderList);

                searchParams.putIfAbsent(type, new HashMap<>());
                if (typeSearchParams != null) {
                    for (Object searchParam : typeSearchParams) {
                        JsonObject searchParamJson = (JsonObject) searchParam;
                        String searchParamName = searchParamJson.getString("name");
                        String searchParamField = searchParamJson.getString("field");
                        if (searchParamName == null || searchParamField == null) {
                            indexFuture.fail("Index config incorrect!");
                            break;
                        }
                        searchParams.get(type).putIfAbsent(searchParamName, searchParamField);
                    }
                }

                indexCreate(type, indexSettingsFilepath, indexCreateResult -> {
                    if (indexCreateResult.failed()
                            && indexCreateResult.cause().getMessage().startsWith("Failed to read")) {
                        indexFuture.fail(indexCreateResult.cause().getMessage());
                    } else {
                        putMapping(type, typeMappingFilepath, putMappingResult -> {
                            if (putMappingResult.failed()
                                    && putMappingResult.cause().getMessage().startsWith("Failed to read")) {
                                indexFuture.fail(putMappingResult.cause().getMessage());
                            } else {
                                parseMapping(type, typeMappingFilepath);
                                indexFuture.complete();
                            }
                        });
                    }
                });
            }
        }

        return indexFutureList;
    }

    private void parseMapping(String type, String typeMappingFilePath) {
        fields.putIfAbsent(type, new HashMap<>());
        vertx.fileSystem().readFile(typeMappingFilePath, ar -> {
            if (ar.succeeded()) {
                parseMapping(
                        new JsonObject(ar.result().toString()).getJsonObject("properties"),
                        this.config.getJsonObject("boost"),
                        fields.get(type),
                        null
                );
            }
        });
    }

    private void parseMapping(JsonObject mapping, JsonObject boost, Map<String, Field> fields, Field parent) {
        for (Map.Entry<String, Object> entry : mapping) {
            JsonObject fieldJson = (JsonObject) entry.getValue();
            Field field = new Field(entry.getKey(), ((JsonObject) entry.getValue()).getString("type"));
            if (boost != null)
                field.setBoost(boost.getFloat(field.getName(), 1.0f));
            JsonObject properties = fieldJson.getJsonObject("properties");

            if (properties != null) {
                field.setSubFields(new ArrayList<>());
                parseMapping(properties, boost, fields, field);
            } else {
                Boolean enabled = fieldJson.getBoolean("enabled");
                String type = fieldJson.getString("type");
                if ((enabled == null || enabled) && (type.equals("text") || type.equals("keyword"))) {
                    field.setSearchable(true);
                    if (parent != null) {
                        parent.setSearchable(true);
                    }
                }
            }

            if (parent == null) {
                fields.put(field.getName(), field);
            } else {
                parent.getSubFields().add(field);
            }
        }
    }

    public void indexCreate(String type, String indexSettingsFilepath, Handler<AsyncResult<String>> handler) {
        vertx.fileSystem().readFile(indexSettingsFilepath, ar -> {
            if (ar.succeeded()) {
                CreateIndexRequest createIndexRequest = new CreateIndexRequest(type);
                createIndexRequest.settings(
                        Settings.builder().loadFromSource(ar.result().toString(), XContentType.JSON)
                );
                client.indices().createAsync(createIndexRequest, RequestOptions.DEFAULT,
                        new ActionListener<CreateIndexResponse>() {

                            @Override
                            public void onResponse(CreateIndexResponse createIndexResponse) {
                                if (createIndexResponse.isAcknowledged() && createIndexResponse.isShardsAcknowledged()) {
                                    handler.handle(Future.succeededFuture(
                                            "The index was successfully created (" + type + ")"));
                                } else {
                                    handler.handle(Future.failedFuture("Failed to create index (" + type + ")"));
                                }
                            }

                            @Override
                            public void onFailure(Exception e) {
                                handler.handle(Future.failedFuture(e));
                            }
                        });
            } else {
                LOG.error("Failed to read settings file: " + ar.cause());
                handler.handle(Future.failedFuture("Failed to read settings file: " + ar.cause().getMessage()));
            }
        });
    }

    public void indexDelete(String type, Handler<AsyncResult<String>> handler) {
        DeleteIndexRequest request = new DeleteIndexRequest(type);
        client.indices().deleteAsync(request, RequestOptions.DEFAULT, new ActionListener<AcknowledgedResponse>() {
            @Override
            public void onResponse(AcknowledgedResponse deleteIndexResponse) {
                if (deleteIndexResponse.isAcknowledged()) {
                    handler.handle(Future.succeededFuture(
                            "The index was successfully deleted (" + type + ")"));
                } else {
                    handler.handle(Future.failedFuture("Failed to delete index (" + type + ")"));
                }
            }

            @Override
            public void onFailure(Exception e) {
                handler.handle(Future.failedFuture(e));
            }
        });
    }

    public void putMapping(String type, String typeMappingFilepath,
                           Handler<AsyncResult<String>> handler) {
        vertx.fileSystem().readFile(typeMappingFilepath, ar -> {
            if (ar.succeeded()) {
                PutMappingRequest putMappingRequest = new PutMappingRequest(type);
                putMappingRequest.type(type);
                putMappingRequest.source(ar.result().toString(), XContentType.JSON);
                client.indices().putMappingAsync(putMappingRequest, RequestOptions.DEFAULT,
                        new ActionListener<AcknowledgedResponse>() {
                            @Override
                            public void onResponse(AcknowledgedResponse putMappingResponse) {
                                if (putMappingResponse.isAcknowledged()) {
                                    handler.handle(Future.succeededFuture(
                                            "The mapping was successfully added (" + type + ")"));
                                } else {
                                    handler.handle(Future.failedFuture("Failed to put mapping (" + type + ")"));
                                }
                            }

                            @Override
                            public void onFailure(Exception e) {
                                handler.handle(Future.failedFuture(e));
                            }
                        });
            } else {
                handler.handle(Future.failedFuture("Failed to read mappings file: " + ar.cause().getMessage()));
            }
        });
    }

    public void boost(String type, String field, Float value, Handler<AsyncResult<String>> handler) {
        String[] keys = field.split("\\.");

        if (fields.get(type) == null) {
            handler.handle(Future.failedFuture("Index doesn't exists"));
        } else if (keys.length == 0) {
            handler.handle(Future.failedFuture("No field provided"));
        } else {
            Field current = fields.get(type).get(keys[0]);

            for (int i = 1; i < keys.length; ++i) {
                if (current.getSubFields() != null) {
                    for (Field subField : current.getSubFields()) {
                        if (subField.getName().equals(keys[i])) {
                            current = subField;
                        }
                    }
                } else {
                    handler.handle(Future.failedFuture("Wrong field name"));
                    return;
                }
            }

            current.setBoost(value);
            handler.handle(Future.succeededFuture(current.toString()));
        }
    }

    public void setMaxAggSize(Integer max_agg_size, Handler<AsyncResult<String>> handler) {
        if (max_agg_size == null) {
            handler.handle(Future.failedFuture("Max aggregation size missing"));
        } else if (max_agg_size <= 0) {
            handler.handle(Future.failedFuture("Max aggregation size has to be greater zero"));
        } else {
            this.max_agg_size = max_agg_size;
            handler.handle(Future.succeededFuture("Successfully set max_agg_size = " + this.max_agg_size));
        }
    }

    private void setMaxResultWindowAsync(Integer max_result_window, Handler<AsyncResult<String>> handler) {
        Settings settings = Settings.builder().put("index.max_result_window", max_result_window).build();
        UpdateSettingsRequest updateSettingsRequest = new UpdateSettingsRequest().settings(settings);
        client.indices().putSettingsAsync(updateSettingsRequest, RequestOptions.DEFAULT,
                new ActionListener<AcknowledgedResponse>() {

                    @Override
                    public void onResponse(AcknowledgedResponse updateSettingsResponse) {
                        if (updateSettingsResponse.isAcknowledged()) {
                            handler.handle(Future.succeededFuture("Successfully set max_result_window = " + max_result_window));
                        } else {
                            handler.handle(Future.failedFuture("Failed to set max_result_window"));
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        handler.handle(Future.failedFuture(e));
                    }
                });
    }

    public void setMaxResultWindow(Integer max_result_window, Handler<AsyncResult<String>> handler) {
        if (max_result_window == null) {
            handler.handle(Future.failedFuture("Max result window missing"));
        } else if (max_result_window < 10000) {
            handler.handle(Future.failedFuture("Max result window has to be greate than 10000"));
        } else {
            setMaxResultWindowAsync(max_result_window, ar -> {
                if (ar.succeeded()) {
                    this.max_result_window = max_result_window;
                    handler.handle(Future.succeededFuture(ar.result()));
                } else {
                    handler.handle(Future.failedFuture(ar.cause()));
                }
            });
        }
    }

    private JsonObject returnSuccess(Integer status, JsonObject result) {
        JsonObject object = new JsonObject();
        if (status != null) object.put("status", status);
        if (result != null) object.put("result", result);
        return object;
    }

    private String returnFailure(Integer status, String message) {
        JsonObject object = new JsonObject();
        if (status != null) object.put("status", status);
        if (message != null) object.put("message", message);
        return object.toString();
    }

    private boolean checkCatalog(JsonObject payload, Handler<AsyncResult<JsonObject>> handler, Future<Void> future,
                                 JsonObject catalog, boolean modify) {
        if (catalog != null) {
            String catalogueId = payload.getJsonObject("catalog").getString("id");
            getCatalogue(catalogueId, ar -> {
                if (ar.succeeded()) {
                    payload.put("catalog", ar.result());
                    payload.put("country", ar.result().getJsonObject("country"));
                    future.complete();
                } else {
                    future.fail(ar.cause());
                }
            });
        } else if (modify) {
            future.complete();
        } else {
            LOG.error("Index dataset: Catalog missing");
            handler.handle(Future.failedFuture(returnFailure(500, "Catalog missing")));
            return true;
        }
        return false;
    }

    private void processBulkResponse(BulkResponse bulkResponse, JsonArray catalogueBulk) {
        int i = 0;
        for (BulkItemResponse bulkItemResponse : bulkResponse.getItems()) {
            if (i >= bulkResponse.getItems().length)
                break;

            while (catalogueBulk.getJsonObject(i) != null &&
                    catalogueBulk.getJsonObject(i).getBoolean("success") != null) {
                i++;
            }

            JsonObject document = catalogueBulk.getJsonObject(i);
            String failure = bulkItemResponse.getFailureMessage();
            if (failure == null) {
                document.put("success", true);
                document.put("status", bulkItemResponse.status().getStatus());
                document.remove("message");
            } else {
                document.put("success", false);
                document.put("status", bulkItemResponse.status().getStatus());
                document.put("message", bulkItemResponse.getFailureMessage());
            }

            i++;

        }
    }

    public void createDataset(JsonObject payload, Handler<AsyncResult<JsonObject>> handler) {
        if (payload == null) {
            handler.handle(Future.failedFuture(returnFailure(500, "Payload is null")));
            return;
        }

        String datasetId = UUID.randomUUID().toString();

        payload.put("id", datasetId);

        JsonObject result = new JsonObject().put("id", datasetId);

        // spatial pre-processing
        if (payload.containsKey("spatial")) {
            JsonObject spatial = payload.getJsonObject("spatial");
            if (spatial != null) {
                payload.put("spatial", Spatial.checkSpatial(payload.getJsonObject("spatial")));
            }
        }

        Future<Void> future = Future.future();
        JsonObject catalog = payload.getJsonObject("catalog");
        if (checkCatalog(payload, handler, future, catalog, false)) return;

        future.setHandler(ar -> {
            if (ar.succeeded()) {
                IndexRequest indexRequest = new IndexRequest("dataset", "dataset", datasetId).opType("create")
                        .source(payload.encode(), XContentType.JSON);
                client.indexAsync(indexRequest, RequestOptions.DEFAULT, new ActionListener<IndexResponse>() {
                    @Override
                    public void onResponse(IndexResponse indexResponse) {
                        LOG.info("Index dataset: Dataset {} created. {}", datasetId, indexResponse.toString());
                        handler.handle(Future.succeededFuture(returnSuccess(201, result)));
                    }

                    @Override
                    public void onFailure(Exception e) {
                        LOG.error("Index dataset: " + e);
                        if (e.getClass().equals(ElasticsearchStatusException.class)) {
                            ElasticsearchStatusException statusException = (ElasticsearchStatusException) e;
                            handler.handle(Future.failedFuture(returnFailure(statusException.status().getStatus(),
                                    statusException.getMessage())));
                        } else {
                            handler.handle(Future.failedFuture(returnFailure(500, e.getMessage())));
                        }
                    }
                });
            } else {
                LOG.error("Index dataset: " + ar.cause());
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
    }

    public void createOrUpdateDataset(String datasetId, JsonObject payload, Handler<AsyncResult<JsonObject>> handler) {
        if (payload == null) {
            handler.handle(Future.failedFuture(returnFailure(500, "Payload is null")));
            return;
        }

        payload.put("id", datasetId);

        JsonObject result = new JsonObject().put("id", datasetId);

        // spatial pre-processing
        if (payload.containsKey("spatial")) {
            JsonObject spatial = payload.getJsonObject("spatial");
            if (spatial != null) {
                payload.put("spatial", Spatial.checkSpatial(payload.getJsonObject("spatial")));
            }
        }

        Future<Void> future = Future.future();
        JsonObject catalog = payload.getJsonObject("catalog");
        if (checkCatalog(payload, handler, future, catalog, false)) return;

        future.setHandler(ar -> {
            if (ar.succeeded()) {
                IndexRequest indexRequest = new IndexRequest("dataset", "dataset", datasetId)
                        .source(payload.encode(), XContentType.JSON);
                client.indexAsync(indexRequest, RequestOptions.DEFAULT, new ActionListener<IndexResponse>() {
                    @Override
                    public void onResponse(IndexResponse indexResponse) {
                        if (indexResponse.status().getStatus() == 200) {
                            // updated
                            LOG.info("Index dataset: Dataset {} updated. {}", datasetId, indexResponse.toString());
                        } else {
                            // created
                            LOG.info("Index dataset: Dataset {} created. {}", datasetId, indexResponse.toString());
                        }
                        handler.handle(Future.succeededFuture(returnSuccess(indexResponse.status().getStatus(), result)));
                    }

                    @Override
                    public void onFailure(Exception e) {
                        LOG.error("Index dataset: " + e);
                        if (e.getClass().equals(ElasticsearchStatusException.class)) {
                            ElasticsearchStatusException statusException = (ElasticsearchStatusException) e;
                            handler.handle(Future.failedFuture(returnFailure(statusException.status().getStatus(),
                                    statusException.getMessage())));
                        } else {
                            handler.handle(Future.failedFuture(returnFailure(500, e.getMessage())));
                        }
                    }
                });
            } else {
                LOG.error("Index dataset: " + ar.cause());
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
    }

    public void modifyDataset(String datasetId, JsonObject payload, Handler<AsyncResult<JsonObject>> handler) {
        if (payload == null) {
            handler.handle(Future.failedFuture(returnFailure(500, "Payload is null")));
            return;
        }

        JsonObject result = new JsonObject().put("id", datasetId);

        // spatial pre-processing
        if (payload.containsKey("spatial")) {
            JsonObject spatial = payload.getJsonObject("spatial");
            if (spatial != null) {
                payload.put("spatial", Spatial.checkSpatial(payload.getJsonObject("spatial")));
            }
        }

        Future<Void> future = Future.future();
        JsonObject catalog = payload.getJsonObject("catalog");
        if (checkCatalog(payload, handler, future, catalog, true)) return;

        future.setHandler(ar -> {
            if (ar.succeeded()) {
                UpdateRequest updateRequest = new UpdateRequest("dataset", "dataset", datasetId)
                        .doc(payload.toString(), XContentType.JSON);
                client.updateAsync(updateRequest, RequestOptions.DEFAULT, new ActionListener<UpdateResponse>() {
                    @Override
                    public void onResponse(UpdateResponse updateResponse ) {
                        LOG.info("Index dataset: Dataset {} modified. {}", datasetId, updateResponse.toString());
                        handler.handle(Future.succeededFuture(returnSuccess(200, result)));
                    }

                    @Override
                    public void onFailure(Exception e) {
                        LOG.error("Index dataset: " + e);
                        if (e.getClass().equals(ElasticsearchStatusException.class)) {
                            ElasticsearchStatusException statusException = (ElasticsearchStatusException) e;
                            handler.handle(Future.failedFuture(returnFailure(statusException.status().getStatus(),
                                    statusException.getMessage())));
                        } else {
                            handler.handle(Future.failedFuture(returnFailure(500, e.getMessage())));
                        }
                    }
                });
            } else {
                LOG.error("Index dataset: " + ar.cause());
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
    }

    public void readDataset(String id, Handler<AsyncResult<JsonObject>> handler) {
        if (id == null) {
            handler.handle(Future.failedFuture(returnFailure(500, "ID is null")));
            return;
        }

        GetRequest getRequest = new GetRequest("dataset", "dataset", id);
        client.getAsync(getRequest, RequestOptions.DEFAULT, new ActionListener<GetResponse>() {
            @Override
            public void onResponse(GetResponse getResponse) {
                if (getResponse.isExists()) {
                    getResponseToJson(getResponse, false,
                            ar -> handler.handle(Future.succeededFuture(returnSuccess(200, ar.result()))));
                } else {
                    LOG.error("Read dataset: Dataset {} not found", id);
                    handler.handle(Future.failedFuture(
                            returnFailure(404, "Dataset " + id + " not found")));
                }
            }

            @Override
            public void onFailure(Exception e) {
                LOG.error("Read dataset: " + e);
                if (e.getClass().equals(ElasticsearchStatusException.class)) {
                    ElasticsearchStatusException statusException = (ElasticsearchStatusException) e;
                    handler.handle(Future.failedFuture(returnFailure(statusException.status().getStatus(),
                            statusException.getMessage())));
                } else {
                    handler.handle(Future.failedFuture(returnFailure(500, e.getMessage())));
                }
            }
        });
    }

    public void deleteDataset(String id, Handler<AsyncResult<JsonObject>> handler) {
        if (id == null) {
            handler.handle(Future.failedFuture(returnFailure(500, "ID is null")));
            return;
        }

        JsonObject result = new JsonObject().put("id", id);

        DeleteRequest deleteRequest = new DeleteRequest("dataset", "dataset", id);
        client.deleteAsync(deleteRequest, RequestOptions.DEFAULT, new ActionListener<DeleteResponse>() {
            @Override
            public void onResponse(DeleteResponse deleteResponse) {
                if (deleteResponse.status() == RestStatus.NOT_FOUND) {
                    LOG.error("Delete dataset: Dataset {} not found", id);
                    handler.handle(Future.failedFuture(
                            returnFailure(404, "Dataset " + id + " not found")));
                } else {
                    handler.handle(Future.succeededFuture(returnSuccess(200, result)));
                }
            }

            @Override
            public void onFailure(Exception e) {
                LOG.error("Delete dataset: " + e);
                if (e.getClass().equals(ElasticsearchStatusException.class)) {
                    ElasticsearchStatusException statusException = (ElasticsearchStatusException) e;
                    handler.handle(Future.failedFuture(returnFailure(statusException.status().getStatus(),
                            statusException.getMessage())));
                } else {
                    handler.handle(Future.failedFuture(returnFailure(500, e.getMessage())));
                }
            }
        });
    }

    public void datasetBulk(JsonArray payload, String operation, Handler<AsyncResult<JsonObject>> handler) {
        if (operation == null || !(operation.equals("create") || operation.equals("replace"))) {
            handler.handle(Future.failedFuture(returnFailure(500, "Operation missing or wrong.")));
            return;
        }

        if (payload == null) {
            handler.handle(Future.failedFuture(returnFailure(500, "Payload is null")));
            return;
        }

        if (payload.isEmpty()) {
            handler.handle(Future.failedFuture(returnFailure(500, "Payload is empty")));
            return;
        }

        CopyOnWriteArrayList<JsonObject> datasetBulk = new CopyOnWriteArrayList<>();

        CopyOnWriteArrayList<IndexRequest> indexRequestList = new CopyOnWriteArrayList<>();

        List<Future> futureList = new ArrayList<>();
        for (Object obj : payload) {
            JsonObject dataset = new JsonObject(obj.toString());

            // check id
            String datasetId;
            if (operation.equals("create")) {
                datasetId = UUID.randomUUID().toString();
            } else {
                datasetId = dataset.getString("id");
            }

            if (datasetId == null) {
                datasetBulk.add(new JsonObject()
                        .put("success", false)
                        .put("status", 400)
                        .put("message", "ID is null")
                        .putNull("id")
                );
                continue;
            }

            // special case dataset, check if catalog exists
            Future bulkFuture = Future.future();
            JsonObject catalog = dataset.getJsonObject("catalog");
            if (catalog != null) {
                String catalogueId = dataset.getJsonObject("catalog").getString("id");
                getCatalogue(catalogueId, ar -> {
                    if (ar.succeeded()) {
                        dataset.put("catalog", ar.result());
                        dataset.put("country", ar.result().getJsonObject("country"));

                        JsonObject doc = new JsonObject()
                                .putNull("success")
                                .putNull("status")
                                .putNull("message")
                                .put("id", datasetId);

                        // spatial pre-processing
                        if (dataset.containsKey("spatial")) {
                            JsonObject spatial = dataset.getJsonObject("spatial");
                            if (spatial != null) {
                                dataset.put("spatial", Spatial.checkSpatial(dataset.getJsonObject("spatial")));
                            }
                        }

                        if (operation.equals("create")) {
                            indexRequestList.add(
                                    new IndexRequest("dataset", "dataset", datasetId)
                                            .source(dataset.toString(), XContentType.JSON).opType("create")
                            );
                        }

                        if (operation.equals("replace")) {
                            indexRequestList.add(
                                    new IndexRequest("dataset", "dataset", datasetId)
                                            .source(dataset.toString(), XContentType.JSON)
                            );
                        }

                        datasetBulk.add(doc);

                        bulkFuture.complete();
                    } else {
                        datasetBulk.add(new JsonObject()
                                .put("success", false)
                                .put("status", 400)
                                .put("message", "Catalog " + catalogueId + " not found")
                                .put("id", datasetId)
                        );
                        bulkFuture.complete();
                    }
                });
            } else {
                datasetBulk.add(new JsonObject()
                        .put("success", false)
                        .put("status", 400)
                        .put("message", "Catalog missing")
                        .put("id", datasetId)
                );
                bulkFuture.complete();
            }

            futureList.add(bulkFuture);
        }

        CompositeFuture.all(futureList).setHandler(ar -> {
            JsonArray datasetBulkJsonArray = new JsonArray();
            for (JsonObject obj : datasetBulk) {
                datasetBulkJsonArray.add(obj);
            }

            BulkRequest bulkRequest = new BulkRequest();
            for (IndexRequest indexRequest : indexRequestList) {
                bulkRequest.add(indexRequest);
            }

            if (bulkRequest.numberOfActions() != 0) {
                client.bulkAsync(bulkRequest, RequestOptions.DEFAULT, new ActionListener<BulkResponse>() {
                    @Override
                    public void onResponse(BulkResponse bulkResponse) {
                        processBulkResponse(bulkResponse, datasetBulkJsonArray);

                        JsonObject result = new JsonObject().put("datasets", datasetBulk);
                        handler.handle(Future.succeededFuture(returnSuccess(200, result)));
                    }

                    @Override
                    public void onFailure(Exception e) {
                        LOG.error("Bulk dataset: " + e);
                        if (e.getClass().equals(ElasticsearchStatusException.class)) {
                            ElasticsearchStatusException statusException = (ElasticsearchStatusException) e;
                            handler.handle(Future.failedFuture(returnFailure(statusException.status().getStatus(),
                                    statusException.getMessage())));
                        } else {
                            handler.handle(Future.failedFuture(returnFailure(500, e.getMessage())));
                        }
                    }
                });
            } else {
                JsonObject result = new JsonObject().put("datasets", datasetBulk);
                handler.handle(Future.succeededFuture(returnSuccess(200, result)));
            }
        });
    }

    public void createCatalogue(JsonObject payload, Handler<AsyncResult<JsonObject>> handler) {
        if (payload == null) {
            handler.handle(Future.failedFuture(returnFailure(500, "Payload is null")));
            return;
        }

        String catalogueId = UUID.randomUUID().toString();

        payload.put("id", catalogueId);

        JsonObject result = new JsonObject().put("id", catalogueId);

        IndexRequest indexRequest = new IndexRequest("catalogue", "catalogue", catalogueId).opType("create")
                .source(payload.encode(), XContentType.JSON);
        client.indexAsync(indexRequest, RequestOptions.DEFAULT, new ActionListener<IndexResponse>() {
            @Override
            public void onResponse(IndexResponse indexResponse) {
                LOG.info("Index catalogue: Catalogue {} created. {}", catalogueId, indexResponse.toString());
                handler.handle(Future.succeededFuture(returnSuccess(201, result)));
            }

            @Override
            public void onFailure(Exception e) {
                LOG.error("Index catalogue: " + e);
                if (e.getClass().equals(ElasticsearchStatusException.class)) {
                    ElasticsearchStatusException statusException = (ElasticsearchStatusException) e;
                    handler.handle(Future.failedFuture(returnFailure(statusException.status().getStatus(),
                            statusException.getMessage())));
                } else {
                    handler.handle(Future.failedFuture(returnFailure(500, e.getMessage())));
                }
            }
        });
    }

    private void updateByQuery(String catalogueId, JsonObject payload, Handler<AsyncResult<BulkByScrollResponse>> handler) {
        UpdateByQueryRequest updateByQueryRequest = new UpdateByQueryRequest("dataset");
        updateByQueryRequest.setConflicts("proceed");

        updateByQueryRequest.setQuery(new TermQueryBuilder("catalog.id.raw", catalogueId));

        StringBuilder script = new StringBuilder();

        if (payload.getJsonObject("country") != null) {
            script.append("ctx._source.country = params.catalog_country;");
        }

        Map<String, Object> params = new HashMap<>();
        //params.putIfAbsent("country", payload.getJsonObject("country").getMap());

        for(Map.Entry<String, Object> entry: payload.getMap().entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();

            script.append(" ctx._source.catalog.");
            script.append(key);
            script.append(" = params.catalog_");
            script.append(key);
            script.append(";");

            // LOG.debug(entry.getValue().getClass().toString());
            // LOG.debug(key);

            if (value.getClass().equals(JsonArray.class)) {
                params.putIfAbsent("catalog_" + key, ((JsonArray) value).getList());
            } else if (value.getClass().equals(JsonObject.class)) {
                params.putIfAbsent("catalog_" + key, ((JsonObject) value).getMap());
            } else {
                params.putIfAbsent("catalog_" + key, value);
            }
        }

        // DEBUG :
        // LOG.debug(script.toString());
        // LOG.debug(params.toString());

        updateByQueryRequest.setScript(
                new Script(
                        ScriptType.INLINE,
                        "painless",
                        script.toString(),
                        params
                )
        );

        client.updateByQueryAsync(updateByQueryRequest, RequestOptions.DEFAULT,
                new ActionListener<BulkByScrollResponse>() {
                    @Override
                    public void onResponse(BulkByScrollResponse bulkResponse) {
                        LOG.info("Index catalogue: Catalogue {} updated all datasets inside.", catalogueId);
                        handler.handle(Future.succeededFuture(bulkResponse));
                    }

                    @Override
                    public void onFailure(Exception e) {
                        LOG.error("Index catalogue: " + e);
                        handler.handle(Future.failedFuture(e));
                    }
                });
    }

    public void createOrUpdateCatalogue(String catalogueId, JsonObject payload, Handler<AsyncResult<JsonObject>> handler) {
        if (payload == null) {
            handler.handle(Future.failedFuture(returnFailure(500, "Payload is null")));
            return;
        }

        payload.put("id", catalogueId);

        JsonObject result = new JsonObject().put("id", catalogueId);

        IndexRequest indexRequest = new IndexRequest("catalogue", "catalogue", catalogueId)
                .source(payload.encode(), XContentType.JSON);
        client.indexAsync(indexRequest, RequestOptions.DEFAULT, new ActionListener<IndexResponse>() {
            @Override
            public void onResponse(IndexResponse indexResponse) {
                if (indexResponse.status().getStatus() == 200) {
                    // updated
                    LOG.info("Index catalogue: Catalogue {} updated. {}", catalogueId, indexResponse.toString());

                    updateByQuery(catalogueId, payload, ar -> {});
                } else {
                    // created
                    LOG.info("Index catalogue: Catalogue {} created. {}", catalogueId, indexResponse.toString());
                }
                handler.handle(Future.succeededFuture(returnSuccess(indexResponse.status().getStatus(), result)));
            }

            @Override
            public void onFailure(Exception e) {
                LOG.error("Index catalogue: " + e);
                if (e.getClass().equals(ElasticsearchStatusException.class)) {
                    ElasticsearchStatusException statusException = (ElasticsearchStatusException) e;
                    handler.handle(Future.failedFuture(returnFailure(statusException.status().getStatus(),
                            statusException.getMessage())));
                } else {
                    handler.handle(Future.failedFuture(returnFailure(500, e.getMessage())));
                }
            }
        });
    }

    public void modifyCatalogue(String catalogueId, JsonObject payload, Handler<AsyncResult<JsonObject>> handler) {
        if (payload == null) {
            handler.handle(Future.failedFuture(returnFailure(500, "Payload is null")));
            return;
        }

        JsonObject result = new JsonObject().put("id", catalogueId);

        UpdateRequest updateRequest = new UpdateRequest("catalogue", "catalogue", catalogueId)
                .doc(payload.toString(), XContentType.JSON);
        client.updateAsync(updateRequest, RequestOptions.DEFAULT, new ActionListener<UpdateResponse>() {
            @Override
            public void onResponse(UpdateResponse updateResponse ) {
                LOG.info("Index catalogue: Catalogue {} modified. {}", catalogueId, updateResponse.toString());

                updateByQuery(catalogueId, payload, ar -> {});

                handler.handle(Future.succeededFuture(returnSuccess(200, result)));
            }

            @Override
            public void onFailure(Exception e) {
                LOG.error("Index catalogue: " + e);
                if (e.getClass().equals(ElasticsearchStatusException.class)) {
                    ElasticsearchStatusException statusException = (ElasticsearchStatusException) e;
                    handler.handle(Future.failedFuture(returnFailure(statusException.status().getStatus(),
                            statusException.getMessage())));
                } else {
                    handler.handle(Future.failedFuture(returnFailure(500, e.getMessage())));
                }
            }
        });
    }

    public void readCatalogue(String catalogueId, Handler<AsyncResult<JsonObject>> handler) {
        if (catalogueId == null) {
            handler.handle(Future.failedFuture(returnFailure(500, "ID is null")));
            return;
        }

        GetRequest getRequest = new GetRequest("catalogue", "catalogue", catalogueId);
        client.getAsync(getRequest, RequestOptions.DEFAULT, new ActionListener<GetResponse>() {
            @Override
            public void onResponse(GetResponse getResponse) {
                if (getResponse.isExists()) {
                    getResponseToJson(getResponse, true, ar ->
                            handler.handle(Future.succeededFuture(returnSuccess(200, ar.result()))));
                } else {
                    LOG.error("Read catalogue: Catalogue {} not found", catalogueId);
                    handler.handle(Future.failedFuture(
                            returnFailure(404, "Catalogue " + catalogueId + " not found")));
                }
            }

            @Override
            public void onFailure(Exception e) {
                LOG.error("Read catalogue: " + e);
                if (e.getClass().equals(ElasticsearchStatusException.class)) {
                    ElasticsearchStatusException statusException = (ElasticsearchStatusException) e;
                    handler.handle(Future.failedFuture(returnFailure(statusException.status().getStatus(),
                            statusException.getMessage())));
                } else {
                    handler.handle(Future.failedFuture(returnFailure(500, e.getMessage())));
                }
            }
        });
    }

    public void deleteCatalogue(String catalogueId, Handler<AsyncResult<JsonObject>> handler) {
        if (catalogueId == null) {
            handler.handle(Future.failedFuture(returnFailure(500, "ID is null")));
            return;
        }

        JsonObject result = new JsonObject().put("id", catalogueId);

        DeleteRequest deleteRequest = new DeleteRequest("catalogue", "catalogue", catalogueId);
        client.deleteAsync(deleteRequest, RequestOptions.DEFAULT, new ActionListener<DeleteResponse>() {
            @Override
            public void onResponse(DeleteResponse deleteResponse) {
                if (deleteResponse.status() == RestStatus.NOT_FOUND) {
                    LOG.error("Delete catalogue: Catalogue {} not found", catalogueId);
                    handler.handle(Future.failedFuture(
                            returnFailure(404, "Catalogue " + catalogueId + " not found")));
                } else {
                    DeleteByQueryRequest deleteByQueryRequest = new DeleteByQueryRequest("dataset");
                    deleteByQueryRequest.setConflicts("proceed");
                    deleteByQueryRequest.setQuery(new TermQueryBuilder("catalog.id.raw", catalogueId));
                    client.deleteByQueryAsync(deleteByQueryRequest, RequestOptions.DEFAULT,
                            new ActionListener<BulkByScrollResponse>() {
                                @Override
                                public void onResponse(BulkByScrollResponse bulkResponse) {
                                    LOG.info("Delete catalogue: Catalogue {} deleted all datasets inside.", catalogueId);
                                }

                                @Override
                                public void onFailure(Exception e) {
                                    LOG.error("Delete catalogue: " + e);
                                }
                            });

                    handler.handle(Future.succeededFuture(returnSuccess(200, result)));
                }
            }

            @Override
            public void onFailure(Exception e) {
                LOG.error("Delete catalogue: " + e);
                if (e.getClass().equals(ElasticsearchStatusException.class)) {
                    ElasticsearchStatusException statusException = (ElasticsearchStatusException) e;
                    handler.handle(Future.failedFuture(returnFailure(statusException.status().getStatus(),
                            statusException.getMessage())));
                } else {
                    handler.handle(Future.failedFuture(returnFailure(500, e.getMessage())));
                }
            }
        });
    }

    public void catalogueBulk(JsonArray payload, String operation, Handler<AsyncResult<JsonObject>> handler) {
        if (operation == null || !(operation.equals("create") || operation.equals("replace"))) {
            handler.handle(Future.failedFuture(returnFailure(500, "Operation missing or wrong.")));
            return;
        }

        if (payload == null) {
            handler.handle(Future.failedFuture(returnFailure(500, "Payload is null")));
            return;
        }

        if (payload.isEmpty()) {
            handler.handle(Future.failedFuture(returnFailure(500, "Payload is empty")));
            return;
        }

        BulkRequest bulkRequest = new BulkRequest();

        JsonArray catalogueBulk = new JsonArray();

        for (Object obj : payload) {
            JsonObject catalogue = new JsonObject(obj.toString());

            // check id
            String catalogueId;
            if (operation.equals("create")) {
                catalogueId = UUID.randomUUID().toString();
            } else {
                catalogueId = catalogue.getString("id");
            }

            if (catalogueId == null) {
                catalogueBulk.add(new JsonObject()
                        .put("success", false)
                        .put("status", 400)
                        .put("message", "ID is null")
                        .putNull("id")
                );
                continue;
            }

            JsonObject doc = new JsonObject()
                    .putNull("success")
                    .putNull("status")
                    .putNull("message")
                    .put("id", catalogueId);

            if (operation.equals("create")) {
                bulkRequest.add(
                        new IndexRequest("catalogue", "catalogue", catalogueId)
                                .source(catalogue.toString(), XContentType.JSON).opType("create")
                );
            }

            if (operation.equals("replace")) {
                bulkRequest.add(
                        new IndexRequest("catalogue", "catalogue", catalogueId)
                                .source(catalogue.toString(), XContentType.JSON)
                );
            }

            catalogueBulk.add(doc);
        }

        if (bulkRequest.numberOfActions() != 0) {
            client.bulkAsync(bulkRequest, RequestOptions.DEFAULT, new ActionListener<BulkResponse>() {
                @Override
                public void onResponse(BulkResponse bulkResponse) {
                    processBulkResponse(bulkResponse, catalogueBulk);

                    JsonObject result = new JsonObject().put("catalogues", catalogueBulk);
                    handler.handle(Future.succeededFuture(returnSuccess(200, result)));
                }

                @Override
                public void onFailure(Exception e) {
                    LOG.error("Bulk catalogue: " + e);
                    if (e.getClass().equals(ElasticsearchStatusException.class)) {
                        ElasticsearchStatusException statusException = (ElasticsearchStatusException) e;
                        handler.handle(Future.failedFuture(returnFailure(statusException.status().getStatus(),
                                statusException.getMessage())));
                    } else {
                        handler.handle(Future.failedFuture(returnFailure(500, e.getMessage())));
                    }
                }
            });
        } else {
            JsonObject result = new JsonObject().put("catalogues", catalogueBulk);
            handler.handle(Future.succeededFuture(returnSuccess(200, result)));
        }
    }

    public void search(String q, Handler<AsyncResult<JsonObject>> handler) {
        Query query = Json.decodeValue(q, Query.class);

        LOG.debug(query.toString());

        Future<SearchResponse> search = doSearch(query);
        Future<SearchResponse> agg = doAggregation(query);

        CompositeFuture.all(search, agg).setHandler(ar -> {
            if(ar.succeeded()) {
                buildResult( search.result(), agg.result(), query,
                        buildResultAr -> handler.handle(
                                Future.succeededFuture(returnSuccess(200, buildResultAr.result()))));
            } else {
                handler.handle(Future.failedFuture(returnFailure(500, ar.cause().getMessage())));
            }
        });
    }

    private Field checkSortField(Field current, String[] path, int i) {
        if (current == null) return null;

        if (path.length == ++i) {
            if (current.getSubFields() == null) {
                return current;
            } else {
                return null;
            }
        } else {
            if (current.getSubFields() != null) {
                Field result = null;
                for (Field subField : current.getSubFields()) {
                    if (subField.getName().equals(path[i])) {
                        result = checkSortField(subField, path, i);
                    }
                }
                return result;
            } else {
                return null;
            }
        }
    }

    private void countDatasets(String query, Handler<AsyncResult<Long>> handler) {
        QueryBuilder termQuery = QueryBuilders.termQuery("catalog.id.raw", query);

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        searchSourceBuilder.query(termQuery);

        SearchRequest searchRequest = new SearchRequest("dataset");
        searchRequest.source(searchSourceBuilder);

        client.searchAsync(searchRequest, RequestOptions.DEFAULT,
                new ActionListener<SearchResponse>() {
                    @Override
                    public void onResponse(SearchResponse searchResponse) {
                        handler.handle(Future.succeededFuture(searchResponse.getHits().getTotalHits()));
                    }

                    @Override
                    public void onFailure(Exception e) {
                        handler.handle(Future.failedFuture(e));
                    }
                });
    }

    private QueryBuilder buildMultiMatch(String querystring, Map<String, Float> multiMatchFields) {
        boolean phrase = false;
        if (querystring.startsWith("\"") && querystring.endsWith("\"")) {
            querystring = querystring.substring(1, querystring.length() - 1);
            phrase = true;
        }

        querystring = querystring.trim();

        if (querystring.isEmpty()) {
            return QueryBuilders.matchAllQuery();
        }

        MultiMatchQueryBuilder multiMatchQuery = QueryBuilders.multiMatchQuery(querystring).lenient(true);

        if (phrase) {
            multiMatchQuery.type(MultiMatchQueryBuilder.Type.PHRASE);
        } else {
            multiMatchQuery.fuzziness(Fuzziness.AUTO);
            multiMatchQuery.prefixLength(4);
            multiMatchQuery.maxExpansions(10);
        }

        multiMatchQuery.fields(multiMatchFields);

        return multiMatchQuery;
    }

    private QueryBuilder parseQueryString(String querystring, Map<String, Float> multiMatchFields) {
        querystring = querystring.trim();

        if (querystring.isEmpty()) {
            return QueryBuilders.matchAllQuery();
        }

        if (querystring.charAt(0) == '(') {
            int open_braces = 1;
            int i = 1;

            for (; i < querystring.length(); ++i) {
                if (querystring.charAt(i) == ')') {
                    open_braces--;
                }
                if (querystring.charAt(i) == '(') {
                    open_braces++;
                }
                if (open_braces == 0) {
                    break;
                }
            }

            if (i == querystring.length() - 1 && open_braces == 0) {
                querystring = querystring.substring(1, querystring.length() - 1);
            }
        }

        if (querystring.isEmpty()) return QueryBuilders.matchAllQuery();

        String[] or_split = querystring.split("(?<!\\()OR\\b(?![\\w\\s]*[)])");

        if (or_split.length == 1) {
            String[] and_split = querystring.split("(?<!\\()AND\\b(?![\\w\\s]*[)])");

            if (and_split.length == 1) {
                return buildMultiMatch(querystring, multiMatchFields);
            } else {
                BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
                for (String s : and_split) {
                    QueryBuilder qb = parseQueryString(s, multiMatchFields);
                    if (qb != null) {
                        boolQuery.must(qb);
                    }
                }
                return boolQuery;
            }
        } else {
            BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
            for (String s : or_split) {
                QueryBuilder qb = parseQueryString(s, multiMatchFields);
                if (qb != null) {
                    boolQuery.should(qb);
                }
            }
            return boolQuery;
        }
    }

    private BoolQueryBuilder buildQuery(Query query) {
        BoolQueryBuilder fullQuery = QueryBuilders.boolQuery();

        if (query.getQ() == null || query.getQ().isEmpty()) {
            QueryBuilder emptyQuery = QueryBuilders.matchAllQuery();
            fullQuery.must(emptyQuery);
        } else {
            QueryBuilder qb = parseQueryString(query.getQ(), determineMultiMatchFields(query));
            fullQuery.must(qb);
        }

        String filter = query.getFilter();

        if (filter != null && !filter.isEmpty() && !filter.equals("autocomplete")) {

            fullQuery.must(QueryBuilders.typeQuery(query.getFilter()));

            if (facets.get(filter) != null && query.getFacets() != null) {
                BoolQueryBuilder facetQuery = QueryBuilders.boolQuery();

                for (String facetName : facets.get(filter).keySet()) {
                    String facetPath = facets.get(filter).get(facetName).getValue();

                    if (query.getFacets().get(facetName) != null && query.getFacets().get(facetName).length != 0) {
                        BoolQueryBuilder subQuery = QueryBuilders.boolQuery();
                        if (query.getFacetOperator().equals(Constants.Operator.AND)) {
                            for (String facet : query.getFacets().get(facetName)) {
                                subQuery.must(QueryBuilders
                                        .termQuery(facetPath + ".id.raw", facet));
                            }
                        } else {
                            for (String facet : query.getFacets().get(facetName)) {
                                subQuery.should(QueryBuilders
                                        .termQuery(facetPath + ".id.raw", facet));
                            }

                            subQuery.minimumShouldMatch(1);
                        }
                        if (query.getFacetGroupOperator().equals(Constants.Operator.AND)) {
                            facetQuery.must(subQuery);
                        } else {
                            facetQuery.should(subQuery);
                        }
                    }
                }

                if (query.getFacetGroupOperator().equals(Constants.Operator.OR))
                    facetQuery.minimumShouldMatch(1);

                fullQuery.must(facetQuery);
            }

            if (query.getSearchParams() != null) {
                if (searchParams.get(filter) != null && !searchParams.get(filter).isEmpty()) {
                    String dateField = searchParams.get(filter).get("temporal");
                    if (dateField != null && !dateField.isEmpty()) {
                        LOG.debug(dateField);
                        Date minDate = query.getSearchParams().getMinDate();
                        Date maxDate = query.getSearchParams().getMaxDate();

                        if (minDate != null && maxDate == null) {
                            fullQuery.must(
                                    QueryBuilders.rangeQuery(dateField)
                                            .gte(minDate)
                            );
                        }
                        if (minDate == null && maxDate != null) {
                            fullQuery.must(
                                    QueryBuilders.rangeQuery(dateField)
                                            .lte(query.getSearchParams().getMaxDate())
                            );
                        }
                        if (minDate != null && maxDate != null) {
                            fullQuery.must(
                                    QueryBuilders.rangeQuery(dateField)
                                            .gte(minDate)
                                            .lte(maxDate)
                            );
                        }
                    }

                    String spatialField = searchParams.get(filter).get("spatial");
                    if (spatialField != null && !spatialField.isEmpty()) {
                        BoundingBox boundingBox = query.getSearchParams().getBoundingBox();

                        if (boundingBox != null) {
                            Float minLon = boundingBox.getMinLon();
                            Float maxLon = boundingBox.getMaxLon();
                            Float maxLat = boundingBox.getMaxLat();
                            Float minLat = boundingBox.getMinLat();

                            if (minLon != null && maxLon != null && maxLat != null && minLat != null) {
                                Coordinate topLeft = new Coordinate(minLon, maxLat);
                                Coordinate bottomRight = new Coordinate(maxLon, minLat);

                                try {
                                    GeoShapeQueryBuilder bbox = geoShapeQuery(
                                            spatialField,
                                            new EnvelopeBuilder(topLeft, bottomRight)
                                    );

                                    fullQuery.filter(bbox);
                                } catch (IOException e) {
                                    // e.printStackTrace();
                                    LOG.warn("Exception while building bounding box.");
                                }
                            }
                        }
                    }
                }
            }
        }

        return fullQuery;
    }

    private Future<SearchResponse> doAggregation(Query query) {
        Future<SearchResponse> future = Future.future();

        BoolQueryBuilder fullQuery = buildQuery(query);

        ArrayList<String> searchIndexes = new ArrayList<>();

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        searchSourceBuilder.query(fullQuery);
        searchSourceBuilder.size(0);

        String filter = query.getFilter();

        if (filter == null || filter.isEmpty() || filter.equals("autocomplete") || facets.get(filter) == null) {
            future.complete(null);
            return future;
        } else {
            searchIndexes.add(filter);

            if (query.getGlobalAggregation()) {
                AggregationBuilder global = AggregationBuilders.global("global");

                for (String facetName : facets.get(filter).keySet()) {
                    if (query.isAggregationAllFields() || query.getAggregationFields().contains(facetName)) {
                        String facetTitle = facets.get(filter).get(facetName).getKey();
                        String facetPath = facets.get(filter).get(facetName).getValue();
                        global.subAggregation(genTermsAggregation(facetPath, facetName, facetTitle));
                    }
                }

                searchSourceBuilder.aggregation(global);
            } else {
                for (String facetName : facets.get(filter).keySet()) {
                    if (query.isAggregationAllFields() || query.getAggregationFields().contains(facetName)) {
                        String facetTitle = facets.get(filter).get(facetName).getKey();
                        String facetPath = facets.get(filter).get(facetName).getValue();
                        searchSourceBuilder.aggregation(
                                genTermsAggregation(facetPath, facetName, facetTitle)
                        );
                    }
                }
            }
        }

        SearchRequest searchRequest = new SearchRequest(searchIndexes.toArray(new String[0]));
        searchRequest.source(searchSourceBuilder);

        client.searchAsync(searchRequest, RequestOptions.DEFAULT, new ActionListener<SearchResponse>() {
            @Override
            public void onResponse(SearchResponse searchResponse) {
                future.complete(searchResponse);
            }

            @Override
            public void onFailure(Exception e) {
                LOG.error("Aggregation: " + e);
                future.fail(e);
            }
        });

        return future;
    }

    private Future<SearchResponse> doSearch(Query query) {
        Future<SearchResponse> future = Future.future();

        BoolQueryBuilder fullQuery = buildQuery(query);

        ArrayList<String> searchIndexes = new ArrayList<>();

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        searchSourceBuilder.query(fullQuery);

        if (query.getFrom() + query.getSize() > max_result_window) {
            LOG.warn("from + size > max_result_window (" + max_result_window + ")");
            searchSourceBuilder.from(0);
            searchSourceBuilder.size(0);
        } else {
            searchSourceBuilder.from(query.getFrom());
            searchSourceBuilder.size(query.getSize());
        }

        // DEBUG
        // searchSourceBuilder.profile(true);

        String filter = query.getFilter();

        List<ImmutablePair<String, SortOrder>> sort = new ArrayList<>();

        if (filter != null && !filter.isEmpty() && !filter.equals("autocomplete")) {
            if (query.getSort() != null && !query.getSort().isEmpty() && fields.get(filter) != null) {
                for (String currentSort : query.getSort()) {
                    String[] sortSplit = currentSort.split("\\+");

                    String sortField = sortSplit[0];
                    SortOrder sortOrder = SortOrder.DESC;

                    if (sortSplit.length >= 2) {
                        if (sortSplit[1].toLowerCase().equals("asc")) {
                            sortOrder = SortOrder.ASC;
                        }
                    }

                    if (sortField.toLowerCase().equals("relevance")) {
                        sort.add(new ImmutablePair<>("relevance", sortOrder));
                    } else {
                        String[] path = sortField.split("\\.");

                        Field result = checkSortField(fields.get(filter).get(path[0]), path, 0);

                        if (result != null) {
                            if (result.getType() != null) {
                                if (result.getType().equals("text")) {
                                    sort.add(new ImmutablePair<>(sortField + ".raw", sortOrder));
                                } else if (result.getType().equals("keyword") || result.getType().equals("date")) {
                                    sort.add(new ImmutablePair<>(sortField, sortOrder));
                                }
                            }
                        }
                    }
                }
            }
        }

        if (!sort.isEmpty()) {
            for (ImmutablePair<String, SortOrder> sortPair : sort) {
                LOG.debug("sort - " + sortPair.getLeft() + " : " + sortPair.getRight().toString());
                if (sortPair.getLeft().equals("relevance")) {
                    searchSourceBuilder.sort(new ScoreSortBuilder().order((sortPair.getRight())));
                } else {
                    searchSourceBuilder.sort(new FieldSortBuilder(sortPair.getLeft()).order((sortPair.getRight())));
                }
            }
        }

        if (filter == null || filter.isEmpty() || filter.equals("autocomplete") || facets.get(filter) == null) {
            searchIndexes.add("dataset");
            searchIndexes.add("catalogue");
        } else {
            searchIndexes.add(filter);
        }

        SearchRequest searchRequest = new SearchRequest(searchIndexes.toArray(new String[0]));
        searchRequest.source(searchSourceBuilder);

        client.searchAsync(searchRequest, RequestOptions.DEFAULT, new ActionListener<SearchResponse>() {
            @Override
            public void onResponse(SearchResponse searchResponse) {
                future.complete(searchResponse);
            }

            @Override
            public void onFailure(Exception e) {
                LOG.error("Search: " + e);
                future.fail(e);
            }
        });

        return future;
    }

    private Map<String, Float> determineMultiMatchFields(Query query) {
        Map<String, Float> multiMatchFields = new HashMap<>();

        if (query.getFilter() != null && !query.getFilter().isEmpty()) {
            if (query.getFilter().equals("autocomplete")) {
                multiMatchFields.put("title.*.autocomplete", 1.0f);
            } else {
                String filter = query.getFilter();
                if (fields.get(filter) != null) {
                    if (query.getFields() == null || query.getFields().isEmpty()) {
                        for (Map.Entry<String, Field> entry : fields.get(filter).entrySet()) {
                            Field field = entry.getValue();
                            addFieldToMultiMatchFields(multiMatchFields, field,
                                    query.getBoost().get(field.getName()));
                        }
                    } else {
                        for (String key : query.getFields()) {
                            Field field = fields.get(filter).get(key);
                            if (field != null) {
                                addFieldToMultiMatchFields(multiMatchFields, field,
                                        query.getBoost().get(field.getName()));
                            }
                        }
                    }
                }


            }
        }

        return multiMatchFields;
    }

    private void addFieldToMultiMatchFields(Map<String, Float> multiMatchFields, Field field, Float boost) {
        if (boost == null) boost = field.getBoost();
        if (field != null && field.isSearchable()) {
            if (field.getSubFields() != null) {
                for (Field subField : field.getSubFields()) {
                    if (subField.isSearchable()) {
                        // multiMatchQuery.field(field.getName() + "." + subField.getName(), boost);
                        if (subField.getSubFields() != null) {
                            for (Field subSubField : subField.getSubFields()) {
                                if (subSubField.isSearchable()) {
                                    multiMatchFields.put(field.getName() + "."
                                            + subField.getName() + "." + subSubField.getName(), boost);
                                }
                            }
                        } else {
                            multiMatchFields.put(field.getName() + "." + subField.getName(), boost);
                        }
                    }
                }
            } else {
                multiMatchFields.put(field.getName(), boost);
            }
        }
    }

    private TermsAggregationBuilder genTermsAggregation(String path, String name, String title) {
        String[] includes = new String[2];
        includes[0] = path + ".id";
        includes[1] = path + ".title";

        return AggregationBuilders
                .terms(name)
                .size(max_agg_size)
                .field(path + ".id.raw")
                .setMetaData(genMetaData(path, title))
                .subAggregation(
                        AggregationBuilders
                                .topHits("topHits")
                                .size(1)
                                .fetchSource(includes, null)
                );
    }

    private Map<String, Object> genMetaData(String path, String title) {
        HashMap<String, Object> metaData = new HashMap<>();
        metaData.put("title", title);
        metaData.put("path", path);
        return metaData;
    }

    private String parseFacetTitle(Object hit, String id, String[] path) {
        if (ArrayList.class.getName().equals(hit.getClass().getName())) {
            for (Object subHit : (ArrayList) hit) {
                if (HashMap.class.getName().equals(subHit.getClass().getName())) {
                    HashMap current;
                    if (path.length > 2) {
                        return "";
                    } else if (path.length == 2) {
                        current = (HashMap) ((HashMap) subHit).get(path[1]);
                    } else {
                        current = (HashMap) subHit;
                    }
                    String currentId = (String) current.get("id");
                    if (currentId != null && currentId.toLowerCase().equals(id)) {
                        String title = (String) current.get("title");
                        if (title != null) {
                            return title;
                        }
                    }
                }
            }
        } else if (HashMap.class.getName().equals(hit.getClass().getName())) {
            HashMap current = (HashMap) hit;
            String currentId = (String) current.get("id");
            if (currentId != null && currentId.toLowerCase().equals(id)) {
                String title = (String) current.get("title");
                if (title != null) {
                    return title;
                }
            }
        }
        return "";
    }

    private void parseCatalogTitle(String id, Handler<AsyncResult<String>> handler) {
        getCatalogue(id, ar -> {
            if (ar.succeeded()) {
                JsonObject catalog = ar.result();
                JsonObject catalog_country = catalog.getJsonObject("country");
                if (catalog_country != null) {
                    String catalog_country_id = catalog_country.getString("id");

                    String title = null;
                    if (catalog_country_id != null && !catalog_country_id.isEmpty()) {
                        JsonObject catalog_title = catalog.getJsonObject("title");
                        if (catalog_title != null) {
                            title = catalog_title.getString(catalog_country_id.toLowerCase());
                        }
                    }

                    if (title == null) {
                        JsonObject catalog_title = catalog.getJsonObject("title");
                        if (catalog_title != null) {
                            title = catalog_title.getString("en");
                        }
                    }

                    if (title == null) {
                        title = id;
                    }

                    handler.handle(Future.succeededFuture(title));
                } else {
                    handler.handle(Future.succeededFuture(id));
                }
            } else {
                handler.handle(Future.succeededFuture(id));
            }
        });
    }

    private void buildResult(SearchResponse searchResponse, SearchResponse aggregationResponse, Query query,
                             Handler<AsyncResult<JsonObject>> handler) {
        JsonObject result = new JsonObject();

        result.put("count", searchResponse.getHits().getTotalHits());

        List<Future> catalogTitleFutureList = new ArrayList<>();

        if (aggregationResponse != null && aggregationResponse.getAggregations() != null) {
            JsonObject facets = new JsonObject();

            Global globalAggregation = aggregationResponse.getAggregations().get("global");

            Aggregations aggregations;
            if (globalAggregation != null) {
                aggregations = globalAggregation.getAggregations();
            } else {
                aggregations = aggregationResponse.getAggregations();
            }

            for (Aggregation agg : aggregations) {
                JsonObject facet = new JsonObject()
                        .put("id", agg.getName())
                        .put("title", agg.getMetaData().get("title"))
                        .put("items", new JsonArray());

                String[] path = ((String) agg.getMetaData().get("path")).split("\\.");

                int count = 0;

                if (agg.getClass().getName().equals(ParsedStringTerms.class.getName())) {
                    for (Terms.Bucket bucket : ((ParsedStringTerms) agg).getBuckets()) {

                        ParsedTopHits topHits = bucket.getAggregations().get("topHits");

                        String id = bucket.getKey().toString().toLowerCase();

                        JsonObject item = new JsonObject()
                                .put("count", bucket.getDocCount())
                                .put("id", id);

                        Future<String> catalogTitleFuture = Future.future();
                        if (agg.getName().equals("catalog")) {
                            parseCatalogTitle(id, ar -> {
                                item.put("title", ar.result());
                                catalogTitleFuture.complete();
                            });
                            catalogTitleFutureList.add(catalogTitleFuture);
                        } else if (path.length >= 1) {
                            for (SearchHit hit : topHits.getHits()) {
                                Map<String, Object> sourceAsMap = hit.getSourceAsMap();
                                String title = parseFacetTitle(sourceAsMap.get(path[0]), id, path);
                                if (!title.isEmpty()) {
                                    item.put("title", title);
                                    break;
                                }
                            }
                        }

                        if ((query.getAggregationLimit() <= 0 || count < query.getAggregationLimit())
                                && bucket.getDocCount() >= query.getAggregationMinCount()) {
                            facet.getJsonArray("items").add(item);
                            count++;
                        }
                    }
                }
                facets.put(facet.getString("id"), facet);
            }

            JsonArray facetsOrdered = new JsonArray();
            LOG.debug(facetOrder.get(query.getFilter()).toString());
            if (facetOrder.get(query.getFilter()) != null) {
                for (String f : facetOrder.get(query.getFilter())) {
                    if (facets.getJsonObject(f) != null) {
                        facetsOrdered.add(facets.getJsonObject(f));
                    }
                }
            }
            result.put("facets", facetsOrdered);
        }

        CompositeFuture.all(catalogTitleFutureList).setHandler(
                catalogTitleFutureResult -> buildResults(searchResponse, query, ar -> {
                    result.put("results", ar.result());
                    handler.handle(Future.succeededFuture(result));
                }));
    }

    private void processCountDatasets(Map<String, Object> sourceAsMap, JsonObject result, Future<Void> countFuture,
                                      String id) {
        countDatasets(sourceAsMap.get("id").toString(), ar -> {
            if (ar.succeeded()) {
                result.put("count", ar.result());
                countFuture.complete();
            } else {
                result.putNull("count");
                LOG.error("Error while counting datasets for catalogue {}", id);
                countFuture.complete();
            }
        });
    }

    private void getCatalogue(String catalogueId, Handler<AsyncResult<JsonObject>> handler) {
        if (catalogueId == null) {
            handler.handle(Future.failedFuture("ID is null"));
            return;
        }

        GetRequest getRequest = new GetRequest("catalogue", "catalogue", catalogueId);
        client.getAsync(getRequest, RequestOptions.DEFAULT, new ActionListener<GetResponse>() {
            @Override
            public void onResponse(GetResponse getResponse) {
                if (getResponse.isExists()) {
                    getResponseToJson(getResponse, false,
                            ar -> handler.handle(Future.succeededFuture(ar.result())));
                } else {
                    handler.handle(Future.failedFuture("Catalogue " + catalogueId + " not found"));
                }
            }

            @Override
            public void onFailure(Exception e) {
                LOG.error("Read catalogue: " + e);
                if (e.getClass().equals(ElasticsearchStatusException.class)) {
                    ElasticsearchStatusException statusException = (ElasticsearchStatusException) e;
                    handler.handle(Future.failedFuture(statusException.getMessage()));
                } else {
                    handler.handle(Future.failedFuture(e.getMessage()));
                }
            }
        });
    }

    private void buildResults(SearchResponse searchResponse, Query query, Handler<AsyncResult<JsonArray>> handler) {
        JsonArray results = new JsonArray();

        SearchHit[] searchHits = searchResponse.getHits().getHits();

        List<Future> futureList = new ArrayList<>();

        for (SearchHit hit : searchHits) {

            Map<String, Object> sourceAsMap = hit.getSourceAsMap();

            JsonObject hitResult = new JsonObject();

            // DEBUG:
            // hitResult.put("_score", hit.getScore())
            // hitResult.put("_id", hit.getId());

            if (query.isElasticId()) {
                hitResult.put("_id", hit.getId());
            }

            DocumentField doc = hit.getFields().get("_ignored");

            List<String> _ignored = new ArrayList<>();

            if (doc != null) {
                doc.getValues().forEach(value -> _ignored.add(value.toString()));
            }

            if (hit.getType().equals("catalogue")) {
                Future<Void> countFuture = Future.future();
                processCountDatasets(sourceAsMap, hitResult, countFuture, hit.getId());
                futureList.add(countFuture);
            }

            for (String type : fields.keySet()) {
                // LOG.debug(type);
                if (hit.getType().equals(type)) {
                    for (String field : fields.get(type).keySet()) {
                        if (_ignored.contains(field)) {
                            // LOG.info("ignored malformed field: " + field);
                            hitResult.putNull(field);
                        } else {
                            // LOG.debug(field);
                            hitResult.put(field, sourceAsMap.get(field));

                            if (field.equals("distributions") && query.isFilterDistributions()
                                    && facets.get("distribution") != null) {
                                for (String facet : facets.get("distribution").keySet()) {
                                    if (query.getFacets().get(facet) != null) {
                                        JsonArray distributions = hitResult.getJsonArray("distributions");
                                        JsonArray distributionsFiltered = new JsonArray();
                                        distributions.forEach(distribution -> {
                                            JsonObject distJson = (JsonObject) distribution;
                                            JsonObject distFacet = distJson.getJsonObject(facet);
                                            if (distFacet != null) {
                                                if (Arrays.asList(query.getFacets().get(facet))
                                                        .contains(distFacet.getString("id"))) {
                                                    distributionsFiltered.add(distJson);
                                                }
                                            }
                                        });
                                        hitResult.put("distributions", distributionsFiltered);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            results.add(hitResult);
        }

        CompositeFuture.all(futureList).setHandler(ar -> handler.handle(Future.succeededFuture(results)));
    }

    private void getResponseToJson(GetResponse getResponse, boolean countDatasets,
                                   Handler<AsyncResult<JsonObject>> handler) {
        Map<String, Object> sourceAsMap = getResponse.getSourceAsMap();

        JsonObject result = new JsonObject();

        // DEBUG:
        // hitResult.put("_score", hit.getScore())
        // hitResult.put("_id", hit.getId());

        DocumentField doc = getResponse.getFields().get("_ignored");

        List<String> _ignored = new ArrayList<>();

        if (doc != null) {
            doc.getValues().forEach(value -> _ignored.add(value.toString()));
        }

        Future<Void> countFuture = Future.future();
        if (getResponse.getType().equals("catalogue") && countDatasets) {
            processCountDatasets(sourceAsMap, result, countFuture, getResponse.getId());
        } else {
            countFuture.complete();
        }

        Future<Void> datasetFuture = Future.future();
        if (!getResponse.getType().equals("dataset")) {
            datasetFuture.complete();
        }

        for (String type : fields.keySet()) {
            // LOG.debug(type);
            if (getResponse.getType().equals(type)) {
                for (String field : fields.get(type).keySet()) {
                    if (_ignored.contains(field)) {
                        // LOG.info("ignored malformed field: " + field);
                        result.putNull(field);
                    } else {
                        // LOG.debug(field);
                        result.put(field, sourceAsMap.get(field));
                    }
                }
            }
        }

        countFuture.setHandler(ar -> handler.handle(Future.succeededFuture(result)));
    }
}
