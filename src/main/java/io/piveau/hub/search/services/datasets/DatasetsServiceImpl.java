package io.piveau.hub.search.services.datasets;

import io.piveau.hub.search.util.ElasticsearchConnector;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class DatasetsServiceImpl implements DatasetsService {

    private ElasticsearchConnector connector;

    DatasetsServiceImpl(ElasticsearchConnector connector, Handler<AsyncResult<DatasetsService>> handler) {
        this.connector = connector;
        handler.handle(Future.succeededFuture(this));
    }

    @Override
    public DatasetsService createDataset(JsonObject payload, Handler<AsyncResult<JsonObject>> handler) {
        connector.createDataset(payload, ar ->  {
            if (ar.succeeded()) {
                handler.handle(Future.succeededFuture(ar.result()));
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
        return this;
    }

    @Override
    public DatasetsService createOrUpdateDataset(String datasetId, JsonObject payload,
                                                 Handler<AsyncResult<JsonObject>> handler) {
        connector.createOrUpdateDataset(datasetId, payload, ar ->  {
            if (ar.succeeded()) {
                handler.handle(Future.succeededFuture(ar.result()));
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
        return this;
    }

    @Override
    public DatasetsService modifyDataset(String datasetId, JsonObject payload,
                                                 Handler<AsyncResult<JsonObject>> handler) {
        connector.modifyDataset(datasetId, payload, ar ->  {
            if (ar.succeeded()) {
                handler.handle(Future.succeededFuture(ar.result()));
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
        return this;
    }

    @Override
    public DatasetsService readDataset(String datasetId, Handler<AsyncResult<JsonObject>> handler) {
        connector.readDataset(datasetId, ar ->  {
            if (ar.succeeded()) {
                handler.handle(Future.succeededFuture(ar.result()));
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
        return this;
    }

    @Override
    public DatasetsService deleteDataset(String datasetId, Handler<AsyncResult<JsonObject>> handler) {
        connector.deleteDataset(datasetId, ar ->  {
            if (ar.succeeded()) {
                handler.handle(Future.succeededFuture(ar.result()));
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
        return this;
    }

    @Override
    public DatasetsService createDatasetBulk(JsonArray payload, Handler<AsyncResult<JsonObject>> handler) {
        connector.datasetBulk(payload,"create", ar ->  {
            if (ar.succeeded()) {
                handler.handle(Future.succeededFuture(ar.result()));
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
        return this;
    }

    @Override
    public DatasetsService createOrUpdateDatasetBulk(JsonArray payload, Handler<AsyncResult<JsonObject>> handler) {
        connector.datasetBulk(payload,"replace", ar ->  {
            if (ar.succeeded()) {
                handler.handle(Future.succeededFuture(ar.result()));
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
        return this;
    }

}
