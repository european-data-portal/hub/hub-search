package io.piveau.hub.search.services.catalogues;

import io.piveau.hub.search.util.ElasticsearchConnector;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class CataloguesServiceImpl implements CataloguesService {

    private ElasticsearchConnector connector;

    CataloguesServiceImpl(ElasticsearchConnector connector, Handler<AsyncResult<CataloguesService>> handler) {
        this.connector = connector;
        handler.handle(Future.succeededFuture(this));
    }

    @Override
    public CataloguesService createCatalogue(JsonObject payload, Handler<AsyncResult<JsonObject>> handler) {
        connector.createCatalogue(payload, ar ->  {
            if (ar.succeeded()) {
                handler.handle(Future.succeededFuture(ar.result()));
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
        return this;
    }

    @Override
    public CataloguesService createOrUpdateCatalogue(String catalogueId, JsonObject payload,
                                                 Handler<AsyncResult<JsonObject>> handler) {
        connector.createOrUpdateCatalogue(catalogueId, payload, ar ->  {
            if (ar.succeeded()) {
                handler.handle(Future.succeededFuture(ar.result()));
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
        return this;
    }

    @Override
    public CataloguesService modifyCatalogue(String catalogueId, JsonObject payload,
                                                 Handler<AsyncResult<JsonObject>> handler) {
        connector.modifyCatalogue(catalogueId, payload, ar ->  {
            if (ar.succeeded()) {
                handler.handle(Future.succeededFuture(ar.result()));
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
        return this;
    }

    @Override
    public CataloguesService readCatalogue(String catalogueId, Handler<AsyncResult<JsonObject>> handler) {
        connector.readCatalogue(catalogueId, ar ->  {
            if (ar.succeeded()) {
                handler.handle(Future.succeededFuture(ar.result()));
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
        return this;
    }

    @Override
    public CataloguesService deleteCatalogue(String catalogueId, Handler<AsyncResult<JsonObject>> handler) {
        connector.deleteCatalogue(catalogueId, ar ->  {
            if (ar.succeeded()) {
                handler.handle(Future.succeededFuture(ar.result()));
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
        return this;
    }

    @Override
    public CataloguesService createCatalogueBulk(JsonArray payload, Handler<AsyncResult<JsonObject>> handler) {
        connector.catalogueBulk(payload,"create", ar ->  {
            if (ar.succeeded()) {
                handler.handle(Future.succeededFuture(ar.result()));
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
        return this;
    }

    @Override
    public CataloguesService createOrUpdateCatalogueBulk(JsonArray payload, Handler<AsyncResult<JsonObject>> handler) {
        connector.catalogueBulk(payload,"replace", ar ->  {
            if (ar.succeeded()) {
                handler.handle(Future.succeededFuture(ar.result()));
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
        return this;
    }

}
