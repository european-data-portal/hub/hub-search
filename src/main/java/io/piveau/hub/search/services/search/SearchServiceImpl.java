package io.piveau.hub.search.services.search;

import io.piveau.hub.search.util.ElasticsearchConnector;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;

public class SearchServiceImpl implements SearchService {

    private ElasticsearchConnector connector;

    SearchServiceImpl(ElasticsearchConnector connector, Handler<AsyncResult<SearchService>> handler) {
        this.connector = connector;
        handler.handle(Future.succeededFuture(this));
    }

    @Override
    public SearchService search(String q, Handler<AsyncResult<JsonObject>> handler) {
        connector.search(q, ar -> {
            if (ar.succeeded()) {
                handler.handle(Future.succeededFuture(ar.result()));
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
        return this;
    }

    @Override
    public SearchService indexCreate(String type, String indexSettingsFilepath,
                                     Handler<AsyncResult<String>> handler) {
        connector.indexCreate(type, indexSettingsFilepath, ar -> {
            if (ar.succeeded()) {
                handler.handle(Future.succeededFuture(ar.result()));
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
        return this;
    }

    @Override
    public SearchService indexDelete(String type, Handler<AsyncResult<String>> handler) {
        connector.indexDelete(type, ar -> {
            if (ar.succeeded()) {
                handler.handle(Future.succeededFuture(ar.result()));
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
        return this;
    }

    @Override
    public SearchService mapping(String type, String typeMappingFilepath,
                                     Handler<AsyncResult<String>> handler) {
        connector.putMapping(type, typeMappingFilepath, ar -> {
            if (ar.succeeded()) {
                handler.handle(Future.succeededFuture(ar.result()));
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
        return this;
    }

    @Override
    public SearchService boost(String type, String field, Float value, Handler<AsyncResult<String>> handler) {
        connector.boost(type, field, value, ar -> {
            if (ar.succeeded()) {
                handler.handle(Future.succeededFuture(ar.result()));
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
        return this;
    }

    @Override
    public SearchService setMaxAggSize(Integer max_agg_size, Handler<AsyncResult<String>> handler) {
        connector.setMaxAggSize(max_agg_size, ar -> {
            if (ar.succeeded()) {
                handler.handle(Future.succeededFuture(ar.result()));
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
        return this;
    }

    @Override
    public SearchService setMaxResultWindow(Integer max_result_window, Handler<AsyncResult<String>> handler) {
        connector.setMaxResultWindow(max_result_window, ar -> {
            if (ar.succeeded()) {
                handler.handle(Future.succeededFuture(ar.result()));
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
        return this;
    }
}
