package io.piveau.hub.search.handler;

import io.piveau.hub.search.models.Constants;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class GazetteerHandler {

    public void autocomplete(RoutingContext context) {
        JsonObject response = new JsonObject();
        context.response().putHeader("Content-Type", "application/json");
        context.response().putHeader("Access-Control-Allow-Origin", "*");
        context.vertx().eventBus().send(Constants.GAZETTEER_AUTOCOMPLETE, context.request().params().get("q"), ar -> {
            if(ar.succeeded()) {
                response.put("success", true);

                response.put("result", new JsonObject(ar.result().body().toString()));

                context.response().setStatusCode(200);
            } else {
                response.put("success", false);
                response.put("message", ar.cause().getMessage());
                context.response().setStatusCode(((ReplyException)ar.cause()).failureCode());
            }
            context.response().end(response.toString());
        });
    }
}
