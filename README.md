# Hub Search

This component creates a search index based on the triple store and enable the UI for searching metadata.

## Prerequisites

* Java JDK 1.8
* Maven 3
* Elasticsearch 6.6.0

## Elasticsearch

* Check if Elasticsearch is running:

```
curl -X GET localhost:9200
{
  "name" : "ehiWcVC",
  "cluster_name" : "docker-cluster",
  "cluster_uuid" : "YfOZBcADQnuurIclVdvsiQ",
  "version" : {
    "number" : "6.6.0",
    "build_flavor" : "default",
    "build_type" : "tar",
    "build_hash" : "a9861f4",
    "build_date" : "2019-01-24T11:27:09.439740Z",
    "build_snapshot" : false,
    "lucene_version" : "7.6.0",
    "minimum_wire_compatibility_version" : "5.6.0",
    "minimum_index_compatibility_version" : "5.0.0"
  },
  "tagline" : "You Know, for Search"
}
```

## Setup

1. Clone repository
2. Navigate into the cloned directory
3. Create the configuration file

```
cp conf/config.sample.json conf/config.json
```

4. Edit the configuration file to your requirements
* **NOTE**: Mandatory fields:
    * `VIADUCT_HUB_SEARCH_API_KEY`
    * `VIADUCT_HUB_SEARCH_ES_CONFIG`
5. Start the application

```
$ mvn package exec:java
```

6. If you started hub-search for the first time go to `http://localhost:8081/shell.html` and run the reset cli command `% reset`

7. Browse to `http://localhost:8080` for api specification

**Note**: The Api-Key and Elasticsearch Config is also configurable through environment variables. 
Then no configuration file is necessary. 
All environment variables have a higher priority then config file variables.

## Build and Run with Docker

build:
```bash
$ mvn clean package
$ docker build -t hub-search .
```

run:
```bash
$ docker-compose up
```

**Note**: The provided docker-compose starts elasticsearch and hub-search

## Result window configuration / Pagination

If from + size > max_result_window an empty results array is returned.

Where "from" equals "page*limit" and "size" equals "limit". 

For the ckan endpoint "from" equals "start" and "size" equals "rows".  

Change max_result_window in the cli by: 

```
% max_result_window value
```

## Config / Environment Variables

* Change the api key of the Application

```
export VIADUCT_HUB_SEARCH_API_KEY="your-api-key"
```

* Change the config of the elasticsearch service

```
export VIADUCT_HUB_SEARCH_ES_CONFIG='{
 "host": "localhost",
 "port": 9200,
 "max_agg_size": 50,
 "max_result_window": 1000000,
 "index": [
   {
     "name" : "dataset",
     "searchAlias": "dataset",
     "type": "dataset",
     "settings": "conf/elasticsearch/settings.json",
     "mapping": "conf/elasticsearch/dataset_mapping.json",
     "facets": [
       {
         "name": "format",
         "title": "Formats",
         "path": "distributions.format"
       },
       {
         "name": "keywords",
         "title": "Keywords",
         "path": "keywords"
       },
       {
         "name": "catalog",
         "title": "Catalogues",
         "path": "catalog"
       },
       {
         "name": "country",
         "title": "Countries",
         "path": "country"
       },
       {
         "name": "licence",
         "title": "Licences",
         "path": "distributions.licence"
       },
       {
         "name": "categories",
         "title": "Categories",
         "path": "categories"
       }
     ],
     "searchParams": [
       {
         "name": "temporal",
         "field": "temporal_coverages"
       },
       {
         "name": "spatial",
         "field": "spatial"
       }
     ]
   },
   {
     "name" : "catalogue",
     "searchAlias": "catalogue",
     "type": "catalogue",
     "settings": "conf/elasticsearch/settings.json",
     "mapping": "conf/elasticsearch/catalogue_mapping.json",
     "facets": [
       {
         "name": "country",
         "title": "Countries",
         "path": "country"
       }
     ],
     "searchParams": [
       {
         "name": "temporal",
         "field": "issued"
       }
     ]
   },
   {
     "name": "distribution",
     "searchAlias": "distribution",
     "type": "distribution",
     "settings": "conf/elasticsearch/settings.json",
     "mapping": "conf/elasticsearch/distribution_mapping.json",
     "facets": [
       {
         "name": "format",
         "title": "Formats",
         "path": "format"
       },
       {
         "name": "licence",
         "title": "Licence",
         "path": "licence"
       }
     ],
     "searchParams": []
   }
 ]
}'
```

* Change the port of the Application

```
export VIADUCT_HUB_SEARCH_SERVICE_PORT=8080
```

* Change the config of the cli service

```
export VIADUCT_HUB_SEARCH_CLI_CONFIG='{
   "port": 8081,
   "type": "http"
}'
```

**Note**: Possible types are http, ssh and telnet

* Change boost parameters for ranking

```
export VIADUCT_HUB_SEARCH_BOOST='{
    "field" : 1.0
}'
```

**Note:** The boost parameters can also be changed via the cli

## Command Line Interface

* Connect via http (username=admin, password=password):

```
https://localhost:8081/shell.html
```

* Connect via ssh (password=password):

```
ssh -p 8081 admin@localhost
```

* Connect via telnet:

```
telnet localhost 8081
```

* Display commands:

```
% help
```

* Reset:

```
% reset
```

* Create index:

```
% index create indexname
```

* Delete index:

```
% index delete indexname
```

* Add mapping:

```
% mapping indexname typename
```

* Boost command:

```
% boost typename fieldname value
```

* Boost example:

```
% boost dataset title 3.0
```

* Load mockdata:

```
% load-mockdata
```

* Increase max_agg_size:

```
% max_agg_size 50
```

* Increase max_result_window:

```
% max_result_window 1000000
```
